#!/usr/bin/env bash

#mvn clean package

echo 'Copy files...'

scp -i ~/.ssh/btrco_key.pem /Users/bogdan.skochynskyi/test/sweater/target/sweater-1.0-SNAPSHOT.jar ubuntu@ec2-18-221-138-109.us-east-2.compute.amazonaws.com:/home/ubuntu/sweater-1.0-SNAPSHOT.jar

echo 'Restart server...'

ssh -i ~/.ssh/btrco_key.pem ubuntu@ec2-18-221-138-109.us-east-2.compute.amazonaws.com  <<EOF

pgrep java | xargs kill -9
nohup java -jar sweater-1.0-SNAPSHOT.jar > log.txt &

EOF

echo 'Bye'