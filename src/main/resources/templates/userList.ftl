<#import "parts/common.ftl" as c>

List of users:
<@c.page>
    <table>
        <thead>
        <tr>
            <th>Name</th>
            <th>Role</th>
            <th></th>
        </tr>
        </thead>

        <#list users as user>
            <tr>
                <td>${user.username}</td>
                <td><#list user.roles as role>${role}<#sep>, </#sep></#list></td>
                <td><a href="/user/${user.id}">edit</a></td>
            </tr>

        </#list>
    </table>
</@c.page>